OC.L10N.register(
    "cospend",
    {
    "Cospend" : "Cospend",
    "Grocery" : "Market",
    "Bar/Party" : "Bar/Parti",
    "Rent" : "Kira",
    "Bill" : "Fatura",
    "Health" : "Sağlık",
    "Shopping" : "Alışveriş",
    "Restaurant" : "Restaurant",
    "Accommodation" : "Konaklama",
    "Transport" : "Ulaşım",
    "Sport" : "Spor",
    "Cospend setting saved" : "Cospend ayarı kayıt edildi",
    "Failed to save Cospend setting" : "Cospend ayarı kayıt edilemedi",
    "Credit card" : "Kredi kartı",
    "Cash" : "Nakit",
    "Check" : "Çek",
    "Transfer" : "Havale",
    "Failed to create project" : "Proje oluşturalamadı",
    "Created member {name}" : "{name} üyesi oluşturuldu",
    "Failed to add member" : "Üye eklenemedi",
    "Reactivate" : "Yeniden aktive et",
    "Deactivate" : "Devre dışı bırak",
    "Member saved" : "Üye kayıt edildi",
    "Failed to save member" : "Üye kayıt edilemedi",
    "Bill created" : "Fatura oluşturuldu",
    "Failed to create bill" : "Fatura oluşturulamadı",
    "Bill saved" : "Fatura kayıt edildi",
    "Failed to save bill" : "Fatura kayıt edilemedi",
    "link" : "link",
    "Deleted {name}" : "{name} silindi",
    "None" : "Hiç biri",
    "Project saved" : "Proje kayıt edildi",
    "Failed to edit project" : "Proje düzenlenemedi",
    "Deleted project {id}" : "{id} projesi silindi",
    "Failed to delete project" : "Proje silinemedi",
    "No bill yet" : "Fatura yok",
    "Deleted bill" : "Fatura silindi",
    "Failed to delete bill" : "Fatura silinemedi",
    "Maximum amount" : "Maksimum tutar",
    "Currency of statistic values" : "İstatistik veri dövizi",
    "Main project's currency" : "Ana proje dövizi",
    "Show disabled members" : "Devre dışı bırakılmış üyeleri göster",
    "Global stats" : "Genel veriler",
    "Monthly stats" : "Aylık veriler",
    "Member/Month" : "Üye/Ay",
    "All members" : "Tüm üyeler",
    "No category" : "Kategori yok",
    "Failed to get bills" : "Faturalar alınamadı",
    "Bill : {what}" : "Fatura : {what}",
    "What?" : "Ne?",
    "Who payed?" : "Kim ödedi?",
    "When?" : "Ne zaman?",
    "For whom?" : "Kim için?",
    "Create the bill" : "Faturayı oluştur",
    "Classic, even split" : "Klasik, eşit dağılım",
    "Bill type" : "Fatura tipi",
    "Include all active member on repeat" : "Tekrarda tüm üyeleri dahil et",
    "Add member" : "Üye ekle",
    "Guest access link" : "Misafir erişim linki",
    "Change password" : "Şifreyi değiştir",
    "Display statistics" : "İstatistikleri göster",
    "Export to csv" : "CSV aktar",
    "Auto export" : "Otomatik dışa aktarma",
    "Manage currencies" : "Dövizleri yönet",
    "Manage categories" : "Kategorileri yönet",
    "Delete" : "Sil",
    "Rename" : "Yeniden adlandır",
    "Only CSV files can be imported" : "Sadece CSV dosyaları içeri aktarılabilinir"
},
"nplurals=2; plural=(n != 1);");
