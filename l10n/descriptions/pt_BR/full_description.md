# Nextcloud Cospend 💰

A Próxima Cospend é um gerenciador de orçamento compartilhado/de grupo. Foi inspirado pelo ótimo [IHateMoney](https://github.com/spiral-project/ihatemoney/).

Você pode usá-lo quando você compartilha uma casa, quando você vai de férias com amigos, ou sempre que você compartilha despesas com outras pessoas.

Ele permite criar projetos com membros e despesas. Cada membro tem um saldo calculado a partir das faturas do projeto. Desta forma você pode ver quem deve ao grupo e a quem o grupo deve. Em última análise pode pedir um plano de quitação que lhe diga quais os pagamentos a fazer para saldar as dívidas dos membros.

Os membros do projeto são independentes dos usuários do Nextcloud. Projetos podem ser acessados e modificados por pessoas sem uma conta do Nextcloud. Cada projeto tem um ID e uma senha para acesso de convidados.

[MoneyBuster](https://gitlab.com/eneiluj/moneybuster) é um cliente Android que está [disponível no F-Droid](https://f-droid.org/packages/net.eneiluj.moneybuster/) e na [Play store](https://play.google.com/store/apps/details?id=net.eneiluj.moneybuster).

[PayForMe](https://github.com/mayflower/PayForMe) iOS client is currently being developped!

## Funcionalidades

* ✎ create/edit/delete projects, members, bills, bill categories, currencies
* ⚖ verificar os saldos dos membros
* 🗠 exibir estatísticas do projeto
* ♻ exibir plano de quitação
* 🎇 criar automaticamente reembolsos a partir do plano de quitação
* 🗓 criar despesas recorrentes (diárias/semanais/mensais/anuais)
* 📊 fornecer, opcionalmente, um valor personalizado para cada membro em despesas novas
* 🔗 link bills with personal files (picture of physical bill for example)
* 👩 acesso de convidado para pessoas fora do Nextcloud
* 👫 share projects with Nextcloud users/groups/circles
* 🖫 importar/exportar projetos no formato CSV (compatível com aquivos CSV do IHateMoney)
* 🔗 generate link/QRCode to easily import projects in MoneyBuster
* 🗲 implement Nextcloud notifications and activity stream

This app is tested on Nextcloud 18 with Firefox 57+ and Chromium.

This app is under development.

🌍 Help us to translate this app on [Nextcloud-Cospend/MoneyBuster Crowdin project](https://crowdin.com/project/moneybuster).

⚒ Check out other ways to help in the [contribution guidelines](https://gitlab.com/eneiluj/cospend-nc/blob/master/CONTRIBUTING.md).

## Instalar

See the [AdminDoc](https://gitlab.com/eneiluj/cospend-nc/wikis/admindoc) for installation details.

Check [CHANGELOG](https://gitlab.com/eneiluj/cospend-nc/blob/master/CHANGELOG.md#change-log) file to see what's new and what's coming in next release.

Check [AUTHORS](https://gitlab.com/eneiluj/cospend-nc/blob/master/AUTHORS.md#authors) file to see complete list of authors.

## Problemas conhecidos

* ele não te torna rico

Any feedback will be appreciated.